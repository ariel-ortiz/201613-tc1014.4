#--------------------------------------------------------------------
# Solicita al usuario su año de nacimiento y calcula su edad
# actual (en el año 2016).
#--------------------------------------------------------------------

year = int(input('Año en que naciste: '))
age = 2016 - year
print('Tu edad es:', age)
