#--------------------------------------------------------------------
# Solicita al usuario una temperatura en grados Farenheit 
# y calcula la temperatura equivalente en grados Celsius y Kelvin.
#--------------------------------------------------------------------

F = float(input("Grados Farenheit: "))
C = (F - 32) * 5 / 9
K = C + 273.15
print(F, 'grados Fahrenheit es igual a', 
      C, 'grados Celsius y a', K, 'Kelvin.')

