# Dibuja la espiral dorada.

from math import sqrt
from turtle import done, circle

fi = 2 / (sqrt(5) - 1)

def espiral(veces):
    radio = 1
    for i in range(veces):
        circle(radio, 90) # Dibuja un cuarto de círculo.
        radio = radio * fi

espiral(13)
done()