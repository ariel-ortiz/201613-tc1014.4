#--------------------------------------------------------------------
# Solicita al usuario un capital inicial, una tasa de interés y un 
# número de periodos. Con eso calcula el capital final resultante.
#--------------------------------------------------------------------

C = float(input("Capital inicial: "))
i = float(input("Tasa de interés: "))
t = int(input("Periodos: "))
F = C * (1 + i) ** t
print("El capital final es", F)