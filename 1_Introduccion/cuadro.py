# Dibuja un cuadro.

from turtle import fd, rt, lt, done, up, down

def cuadro(lado):
    for i in range(4):
        print(i, lado)
        fd(lado)
        rt(90)
        
def cuatro_cuadros(lado):
    for i in range(4):
        cuadro(lado)
        rt(90)
        
def escalera(lado, veces):
    for i in range(veces):
        cuadro(lado)
        up()
        fd(lado)
        lt(90)
        fd(lado)
        rt(90)
        down()
    

escalera(50, 5)
done()