# Solución al problema 1 del
# examen práctico #2.

def poblacion_total(I, C, n):
    T = I * C ** n
    return T

def main():
    I = int(input('Población incial: '))
    C = int(input('Número de descendientes por progenitor: '))
    n = int(input('Número de generación: '))
    T = poblacion_total(I, C, n)
    print('La población total es', T)

main()