# Ejemplo sencillo usando ciclos for para repetir
# múltiples veces una misma salida.

def main():
    print('Inicio')
    for i in range(5):
        print(i, "Hola")
    for i in range(5):
        print('Fin')

main()