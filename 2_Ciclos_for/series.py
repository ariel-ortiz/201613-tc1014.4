# Ejemplos de series matemáticas implementadas
# con ciclos for.

from math import pi, factorial, e

def pi_aprox(n):
    s = 0
    for i in range(n + 1):
        s += (-1) ** i / (2 * i + 1)
    return s * 4

def coseno(x, n):
    s = 0
    for i in range(n + 1):
        s += ((-1) ** i * x ** (2 * i)) / factorial(2 * i)
    return s

def e_aprox(x, n):
    s = 0
    for i in range(n + 1):
        s += (x ** i) / factorial(i)
    return s

def main():
    print(pi)
    print(pi_aprox(100))
    print(coseno(0, 10))
    print(coseno(pi, 20))
    print(coseno(pi / 2, 20))
    print(coseno(pi / 3, 20))
    print(e)
    print(e_aprox(1, 10))

main()