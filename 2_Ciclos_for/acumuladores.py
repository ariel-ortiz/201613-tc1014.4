# Demuestra el uso de ciclos for usando
# acumuladores.

def sumatoria_consecutiva(n):
    s = 0  # s es un acumulador.
    for x in range(1, n + 1):
        s = s + x
    return s

def sumatoria_gauss(n):
    return int((n / 2) * (n + 1))

def factorial(n):
    m = 1 # m es un acumulador.
    for x in range(1, n + 1):
        m = m * x
    return m

def main():
    n = 101
    r1 = sumatoria_consecutiva(n)
    r2 = sumatoria_gauss(n)
    print(r1)
    print(r2)
    n = 5
    r3 = factorial(n)
    print(r3)

main()