# Ejemplos de manipulación de archivos.

# Leer un archivo todo a la vez usando el método read().
def prueba1():
    with open('prueba.txt') as f:
        s = f.read()
    print(s)
    
# Leer un archivo, una línea a la vez, usando el método readline().
def prueba2():
    with open('prueba.txt') as f:
        s = 'x'
        while s != '':
            s = f.readline()
            if s != '':
                print(s.rstrip())
   
# Leer un archivo todo a la vez usando el método readlines().             
def prueba3():
    with open('prueba.txt') as f:
        r = f.readlines()
    for linea in r:
        print(linea, end='')

# Leer un archivo, una línea a la vez, usando un for.
def prueba4():
    with open('prueba.txt') as f:
        for linea in f:
            print(linea, end='')
    
# Crear un archivo nuevo.        
def prueba5():
    with open('dieces.txt', 'w') as f:
        for i in range(10, 1001, 10):
            f.write(str(i) + '\n')

# Generar un nuevo archivo a partir de la concatenación de dos
# archivos existentes.
def prueba6():
    with open('uno.txt') as f1, open('dos.txt') as f2, open('tres.txt', 'w') as f3:
        contenido1 = f1.read()
        contenido2 = f2.read()
        f3.write(contenido1)
        f3.write(contenido2)

def main():
    #prueba1()
    #prueba2()
    #prueba3()
    #prueba4()
    #prueba5()
    prueba6()
        
main()