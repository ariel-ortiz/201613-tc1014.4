# Programa que calcula el índice de masa
# corporal de una persona a partir
# de su peso en kilos y estatura en metros.

def bmi(weight, height):
    index = weight / height ** 2
    if index < 20:
        return 'underweight'
    elif index < 25:
        return 'normal'
    elif index < 30:
        return 'obese1'
    elif index < 40:
        return 'obese2'
    else:
        return 'obese3'

def main():
    print(bmi(56, 1.9))
    print(bmi(60, 1.6))
    print(bmi(80, 1.65))
    print(bmi(150, 1.4))

main()