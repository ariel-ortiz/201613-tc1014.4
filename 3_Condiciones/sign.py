# La función sign devuele:
# -1 si n es negativo
# 1 si n es positivo mayor a cero
# 0 si n es cero.

def sign(n):
    if n < 0:
        return -1
    if n > 0:
        return 1
    return 0

def main():
    print(sign(-10))
    print(sign(5))
    print(sign(0))

main()