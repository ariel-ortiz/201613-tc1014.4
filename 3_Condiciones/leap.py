# Programa de manipulación de fechas que
# utiliza instrucciones condicionales.

def leap(y):
    if y % 4 == 0:
        if y % 100 == 0:
            if y % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False

def num_dias_mes(m, y):
    if m in [1, 3, 5, 7, 8, 10, 12]:
        return 31
    elif m == 2:
        if leap(y):
            return 29
        else:
            return 28
    else:
        return 30

def next_day(y, m, d):
    if m == 12 and d == num_dias_mes(m, y):
        return y + 1, 1, 1
    elif d == num_dias_mes(m, y):
        return y, m + 1, 1
    else:
        return y, m, d + 1

def main():
    print(leap(2015))
    print(leap(2016))
    print(leap(2000))
    print(leap(2100))
    print(next_day(2015, 2, 13))
    print(next_day(2015, 2, 28))
    print(next_day(2016, 2, 28))
    print(next_day(2016, 12, 31))

main()