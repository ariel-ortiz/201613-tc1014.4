# Cuarto examen práctico
# Solución al problema 3

def tabla(n, archivo_salida):
    with open(archivo_salida, 'w') as f:
        for i in range(1, 11):
            f.write('{0} x {1} = {2}\n'.format(n, i, n*i))

def main():
    tabla(2, 'tabla2.txt')
    tabla(7, 'tabla7.txt')

main()