# Cuarto examen práctico
# Solución al problema 2

def vocales(archivo_entrada):
    with open(archivo_entrada) as f:
        s = f.read().split()
    c = 0
    for p in s:
        if p[0].upper() in 'AEIOU':
            c += 1
    return c

def main():
    print(vocales('entrada1.txt'))
    print(vocales('entrada2.txt'))

main()