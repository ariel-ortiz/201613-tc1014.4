# Cuarto examen práctico
# Solución al problema 1

def sietes(n):
    r = []
    for i in range(7, n + 1):
        if i % 7 == 0 or i % 10 == 7:
            r.append(i)
    return r

def main():
    print(sietes(20))
    print(sietes(50))
    print(sietes(87))
    print(sietes(6))

main()