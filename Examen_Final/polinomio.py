# Examen final práctico
# Solución al problema 3

def polinomio(x, coefs):
    resultado = 0
    exponente = len(coefs) - 1
    for c in coefs:
        resultado += c * x ** exponente
        exponente -= 1
    return resultado

def main():
    print(polinomio(3.14, [1]))
    print(polinomio(3.0, [4, 3, -3]))
    print(polinomio(2.5, [-2, 3, -1, 0, 5]))
    print(polinomio(-1.0, [1, 1, 1, 1, 1, 1]))

main()