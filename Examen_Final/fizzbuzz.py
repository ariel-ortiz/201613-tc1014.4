# Examen final práctico
# Solución al problema 1

def fizzbuzz(n):
    resultado = []
    for i in range(1, n + 1):
        if i % 3 == 0 and i % 5 == 0:
            resultado.append('FizzBuzz')
        elif i % 3 == 0:
            resultado.append('Fizz')
        elif i % 5 == 0:
            resultado.append('Buzz')
        else:
            resultado.append(i)
    return resultado

def main():
    print(fizzbuzz(1))
    print(fizzbuzz(5))
    print(fizzbuzz(15))
    print(fizzbuzz(60))

main()
