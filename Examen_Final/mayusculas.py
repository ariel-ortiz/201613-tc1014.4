# Examen final práctico
# Solución al problema 2

def mayusculas(archivo_entrada, archivo_salida):
    with open(archivo_entrada) as entrada, open(archivo_salida, 'w') as salida:
        s = entrada.read()
        salida.write(s.upper())

def main():
    mayusculas('entrada1.txt', 'salida1.txt')
    mayusculas('entrada2.txt', 'salida2.txt')

main()