# Examen final práctico
# Solución al problema 4 (opcional)

def moda(archivo_entrada):
    palabras = {}
    with open(archivo_entrada) as a:
        for p in a:
            p = p.strip()
            palabras[p] = palabras.get(p, 0) + 1
    mayor = max(palabras.values())
    resultado = []
    for k in palabras:
        if palabras[k] == mayor:
            resultado.append(k)
    resultado.sort()
    return resultado

def antimoda(archivo_entrada):
    palabras = {}
    with open(archivo_entrada) as a:
        for p in a:
            p = p.strip()
            palabras[p] = palabras.get(p, 0) + 1
    menor = min(palabras.values())
    resultado = []
    for k in palabras:
        if palabras[k] == menor:
            resultado.append(k)
    resultado.sort()
    resultado.reverse()
    return resultado

def main():
    print(moda('prueba1.txt'))
    print(moda('prueba2.txt'))
    print(moda('prueba3.txt'))
    print(moda('prueba4.txt'))
    print(moda('prueba5.txt'))
    print(moda('prueba6.txt'))
    print()
    print(antimoda('prueba1.txt'))
    print(antimoda('prueba2.txt'))
    print(antimoda('prueba3.txt'))
    print(antimoda('prueba4.txt'))
    print(antimoda('prueba5.txt'))
    print(antimoda('prueba6.txt'))

main()