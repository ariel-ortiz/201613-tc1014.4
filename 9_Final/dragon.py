# Dibuja las curvas del dragón.
#
# Algoritmo diseñado por John Heighway, Bruce Banks y
# William Harter, físicos de la NASA.

from turtle import fd, lt, rt, done, speed, ht

def inversa(c):
    r = ''
    for x in c:
        if x == 'I':
            r += 'D'
        else:
            r += 'I'
    return r

def curva(n):
    r = ''
    for i in range(n):
        r = r + 'I' + inversa(r[::-1])
    return r

def dragon(n, d):
    fd(d)
    for i in curva(n):
        if i == 'I':
            lt(90)
        else:
            rt(90)
        fd(d)

def main():
    speed(0)
    ht()
    dragon(11, 4)
    done()

main()