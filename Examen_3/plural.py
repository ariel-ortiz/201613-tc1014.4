# Tercer examen práctico
# Solución al problema 3

def plural(palabra):
    if palabra[-1] in 'aeiou':
        return palabra + 's'
    elif palabra[-1] == 'z':
        return palabra[:-1] + 'ces'
    else:
        return palabra + 'es'

def main():
    print(plural('gato'))
    print(plural('puerta'))
    print(plural('zopilote'))
    print(plural('nuez'))
    print(plural('codorniz'))
    print(plural('cruz'))
    print(plural('animal'))
    print(plural('complot'))
    print(plural('hobits'))

main()
