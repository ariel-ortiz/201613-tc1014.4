# Tercer examen práctico
# Solución al problema 2

def cuenta_unos(cadena):
    s = 0
    for c in cadena:
        if c == '1':
            s += 1
    return s

def paridad(bits):
    n = cuenta_unos(bits)
    if n % 2 == 0:
        return bits + '0'
    else:
        return bits + '1'

def main():
    print(paridad('1000110'))
    print(paridad('1011001'))
    print(paridad('0000000'))
    print(paridad('1111111'))
    print(paridad('0010000'))
    print(paridad('1101111'))

main()
