# Funciones ejemplo de manipulación de cadenas de caracteres.

def cuenta_vocales(cadena):
    s = 0
    for c in cadena:
        if c in 'aeiouAEIOU':
            s += 1
    return s

def inserta_espacios(cadena):
    resultado = ''
    for c in cadena:
        resultado += c + ' '
    return resultado[:-1]

def binario(n):
    if n == 0:
        return '0'
    if n < 0:
        negativo = True
    else:
        negativo = False
    n = abs(n)
    r = ''
    while n != 0:
        r = str(n % 2) + r
        n //= 2
    if negativo:
        r = '-' + r
    return r

def main():
    print(cuenta_vocales('hola todos'))
    print(cuenta_vocales('xyz'))
    print(cuenta_vocales(''))
    print(cuenta_vocales('ADIOS'))
    print(inserta_espacios('hola a todos'))
    print(binario(25))
    print(binario(0))

main()