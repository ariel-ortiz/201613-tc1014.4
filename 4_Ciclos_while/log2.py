# Calcula, como número entero, el logaritmo base 2 de n.
# En otras palabras, cuenta el número de veces que se puede
# dividir n entre 2.

def log2(n):
    c = -1
    while n != 0:
        n //= 2
        c += 1
    return c

def main():
    print(log2(2))
    print(log2(1))
    print(log2(0))
    print(log2(1024))
    print(log2(1023))

main()