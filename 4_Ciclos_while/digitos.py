# Imprime los dígitos de un número, cada dígito en su propia
# línea y en orden de inverso (el dígito de la derecha primero).

def digitos(n):
    while n != 0:
        r = n % 10
        n //= 10
        print(r)
        
def main():
    digitos(354)
    print()
    digitos(1000)
    print()
    digitos(7)
    print()
    digitos(123456789)
    
main()