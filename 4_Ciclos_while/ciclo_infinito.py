# Ejemplo de un ciclo infinito.

def main():
    i = 1
    while i <= 10:
        print(i)
        # Sin la siguiente línea el programa nunca termina.
        # i += 1
        
main()